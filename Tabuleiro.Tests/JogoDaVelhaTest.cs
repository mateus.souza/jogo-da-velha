using JogoDaVelhaDll;
using Xunit;
using System;
namespace Tabuleiro.Tests
{
    public class JogoDaVelhaTest
    {
        [Fact]
        public void TestarSePosicaoEstaOcupada()
        {
            Jogador jogador = new Jogador('X');
            JogoDaVelha jogodavelha = new JogoDaVelha();
            jogodavelha.Jogar(1,2, jogador);
            Assert.Throws<ArgumentException>(() => jogodavelha.Jogar(1, 2, jogador));
        }

        [Fact]
        public void TestarSePosicaoTemTamanhoInvalido()
        {
            Jogador jogador = new Jogador('X');
            JogoDaVelha jogodavelha = new JogoDaVelha();
            Assert.Throws<IndexOutOfRangeException>(() => jogodavelha.Jogar(4, 4, jogador));

        }

        [Fact]
        public void TestarSeGanhouHorizontal1()
        {
            Jogador jogador = new Jogador('X');
            JogoDaVelha jogodavelha = new JogoDaVelha();

            jogodavelha.Jogar(0, 0, jogador);
            jogodavelha.Jogar(0, 1, jogador);
            jogodavelha.Jogar(0, 2, jogador);

            Assert.True(jogodavelha.VerificarResultado(jogador));
        }

        [Fact]
        public void TestarSeGanhouHorizontal2()
        {
            Jogador jogador = new Jogador('X');
            JogoDaVelha jogodavelha = new JogoDaVelha();

            jogodavelha.Jogar(1, 0, jogador);
            jogodavelha.Jogar(1, 1, jogador);
            jogodavelha.Jogar(1, 2, jogador);

            Assert.True(jogodavelha.VerificarResultado(jogador));
        }

        [Fact]
        public void TestarSeGanhouHorizontal3()
        {
            Jogador jogador = new Jogador('X');
            JogoDaVelha jogodavelha = new JogoDaVelha();

            jogodavelha.Jogar(2, 0, jogador);
            jogodavelha.Jogar(2, 1, jogador);
            jogodavelha.Jogar(2, 2, jogador);

            Assert.True(jogodavelha.VerificarResultado(jogador));
        }

        [Fact]
        public void TestarSeGanhouVertical1()
        {
            Jogador jogador = new Jogador('X');
            JogoDaVelha jogodavelha = new JogoDaVelha();

            jogodavelha.Jogar(0, 0, jogador);
            jogodavelha.Jogar(1, 0, jogador);
            jogodavelha.Jogar(2, 0, jogador);

            Assert.True(jogodavelha.VerificarResultado(jogador));
        }

        [Fact]
        public void TestarSeGanhouVertical2()
        {
            Jogador jogador = new Jogador('X');
            JogoDaVelha jogodavelha = new JogoDaVelha();

            jogodavelha.Jogar(0, 1, jogador);
            jogodavelha.Jogar(1, 1, jogador);
            jogodavelha.Jogar(2, 1, jogador);

            Assert.True(jogodavelha.VerificarResultado(jogador));
        }

        [Fact]
        public void TestarSeGanhouVertical3()
        {
            Jogador jogador = new Jogador('X');
            JogoDaVelha jogodavelha = new JogoDaVelha();

            jogodavelha.Jogar(0, 2, jogador);
            jogodavelha.Jogar(1, 2, jogador);
            jogodavelha.Jogar(2, 2, jogador);

            Assert.True(jogodavelha.VerificarResultado(jogador));
        }

        [Fact]
        public void TestarSeGanhouDiagonal1()
        {
            Jogador jogador = new Jogador('X');
            JogoDaVelha jogodavelha = new JogoDaVelha();

            jogodavelha.Jogar(0, 0, jogador);
            jogodavelha.Jogar(1, 1, jogador);
            jogodavelha.Jogar(2, 2, jogador);

            Assert.True(jogodavelha.VerificarResultado(jogador));
        }

        [Fact]
        public void TestarSeGanhouDiagonal2()
        {
            Jogador jogador = new Jogador('X');
            JogoDaVelha jogodavelha = new JogoDaVelha();

            jogodavelha.Jogar(0, 2, jogador);
            jogodavelha.Jogar(1, 1, jogador);
            jogodavelha.Jogar(2, 0, jogador);

            Assert.True(jogodavelha.VerificarResultado(jogador));
        }

        [Fact]
        public void TestarSeEmpatou()
        {
            Jogador jogador = new Jogador('X');
            JogoDaVelha jogodavelha = new JogoDaVelha();

            jogodavelha.Jogar(0, 0, jogador);
            jogodavelha.Jogar(0, 1, jogador);
            jogodavelha.Jogar(0, 2, jogador);
            jogodavelha.Jogar(1, 0, jogador);
            jogodavelha.Jogar(1, 1, jogador);
            jogodavelha.Jogar(1, 2, jogador);
            jogodavelha.Jogar(2, 0, jogador);
            jogodavelha.Jogar(2, 1, jogador);
            jogodavelha.Jogar(2, 2, jogador);

            Assert.True(jogodavelha.VerificarSeEmpatou());
        }
    }
}