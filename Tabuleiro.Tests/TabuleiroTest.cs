using System;
using JogoDaVelhaDll;
using Xunit;

namespace Tabuleiro.Tests
{
    public class UnitTest1
    {
        [Fact]
        public void TestarSePosicaoEstaOcupada()
        {
            JogoDaVelhaDll.Tabuleiro tabuleiro = new JogoDaVelhaDll.Tabuleiro();
            tabuleiro.armazenarPosicao(1,2, 'x');
            Assert.Throws<ArgumentException>(()=> tabuleiro.armazenarPosicao(1,2, 'x'));
        }

         [Fact]
        public void TestarSePosicaoTemTamanhoInvalido()
        {
            JogoDaVelhaDll.Tabuleiro tabuleiro = new JogoDaVelhaDll.Tabuleiro();
            Assert.Throws<IndexOutOfRangeException>(()=> tabuleiro.armazenarPosicao(4,4, 'x'));
        
        }

        [Fact]
        public void TestarSeGanhouHorizontal1()
        {
            Jogador jogador = new Jogador('X');
            JogoDaVelhaDll.Tabuleiro tabuleiro = new JogoDaVelhaDll.Tabuleiro();
            jogador.Jogar(0,0, tabuleiro);
            jogador.Jogar(0,1, tabuleiro);
            jogador.Jogar(0,2, tabuleiro);

            Assert.True(tabuleiro.VerificaSeGanhou());
        }

         [Fact]
        public void TestarSeGanhouHorizontal2()
        {
            Jogador jogador = new Jogador('X');
            JogoDaVelhaDll.Tabuleiro tabuleiro = new JogoDaVelhaDll.Tabuleiro();
            jogador.Jogar(1,0, tabuleiro);
            jogador.Jogar(1,1, tabuleiro);
            jogador.Jogar(1,2, tabuleiro);

            Assert.True(tabuleiro.VerificaSeGanhou());
        }

         [Fact]
        public void TestarSeGanhouHorizontal3()
        {
            Jogador jogador = new Jogador('X');
            JogoDaVelhaDll.Tabuleiro tabuleiro = new JogoDaVelhaDll.Tabuleiro();
            jogador.Jogar(2,0, tabuleiro);
            jogador.Jogar(2,1, tabuleiro);
            jogador.Jogar(2,2, tabuleiro);

            Assert.True(tabuleiro.VerificaSeGanhou());
        }

         [Fact]
        public void TestarSeGanhouVertical1()
        {
            Jogador jogador = new Jogador('X');
            JogoDaVelhaDll.Tabuleiro tabuleiro = new JogoDaVelhaDll.Tabuleiro();
            jogador.Jogar(0,0, tabuleiro);
            jogador.Jogar(1,0, tabuleiro);
            jogador.Jogar(2,0, tabuleiro);

            Assert.True(tabuleiro.VerificaSeGanhou());
        }

         [Fact]
        public void TestarSeGanhouVertical2()
        {
            Jogador jogador = new Jogador('X');
            JogoDaVelhaDll.Tabuleiro tabuleiro = new JogoDaVelhaDll.Tabuleiro();
            jogador.Jogar(0,1, tabuleiro);
            jogador.Jogar(1,1, tabuleiro);
            jogador.Jogar(2,1, tabuleiro);

            Assert.True(tabuleiro.VerificaSeGanhou());
        }

         [Fact]
        public void TestarSeGanhouVertical3()
        {
            Jogador jogador = new Jogador('X');
            JogoDaVelhaDll.Tabuleiro tabuleiro = new JogoDaVelhaDll.Tabuleiro();
            jogador.Jogar(0,2, tabuleiro);
            jogador.Jogar(1,2, tabuleiro);
            jogador.Jogar(2,2, tabuleiro);

            Assert.True(tabuleiro.VerificaSeGanhou());
        }

        public void TestarSeEmpatouValido()
        {
            JogoDaVelhaDll.Tabuleiro tabuleiro = new JogoDaVelhaDll.Tabuleiro();

            Assert.Throws<ArgumentException>(()=> tabuleiro.armazenarPosicao(0,0, 'x'));
            Assert.Throws<ArgumentException>(()=> tabuleiro.armazenarPosicao(0,1, 'x'));
            Assert.Throws<ArgumentException>(()=> tabuleiro.armazenarPosicao(0,2, 'x'));
        
        }
    }
}
