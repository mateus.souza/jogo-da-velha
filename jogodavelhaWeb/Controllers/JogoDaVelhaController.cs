﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using JogoDaVelhaDll;

namespace jogodavelhaWeb.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class JogoDaVelhaController : ControllerBase
    {
        IJogoDaVelha jogodavelha = JogoDaVelha.Criar();
       
        public JogoDaVelhaController(IJogoDaVelha jogo)
        {
            jogodavelha = jogo;
        }

        [HttpPost]
        public void Jogar(Entrada entrada)
        {
            jogodavelha.Jogar(entrada.x, entrada.y, new NovoJogador(entrada.simbolo).ParaJogador());
        }

        [HttpGet]
        public bool VerificarResultado(NovoJogador jogador)
        {
            return jogodavelha.VerificarResultado(jogador.ParaJogador());
        }

        [HttpGet("Verificar se empatou")]
        public bool VerificarSeEmpatou()
        {
            return jogodavelha.VerificarSeEmpatou();
        }

        [HttpGet("Imprimir tabuleiro")]
        public string Imprimir()
        {
            return jogodavelha.ImprimirTabuleiro();
        }

        
       
    }
}
