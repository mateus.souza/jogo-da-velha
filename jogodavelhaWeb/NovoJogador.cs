using JogoDaVelhaDll;
using System;

namespace jogodavelhaWeb
{
    public class NovoJogador
    {
        private char simbolo;
        public NovoJogador(char simbolo)
        {   
            this.simbolo = simbolo;
        }
        public Jogador ParaJogador()
        {
            return new Jogador(simbolo); 
        }
    }
}
