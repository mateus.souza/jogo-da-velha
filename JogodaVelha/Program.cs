﻿using System;
using JogoDaVelhaDll;

namespace Jogo_da_Velha2
{
    class Program
    {
        static void Main(string[] args)
        {
            IJogoDaVelha jogodavelha = JogoDaVelha.Criar();
            Tabuleiro tabuleiro = new Tabuleiro();
            
            Console.WriteLine("Digite um char identificador para o Jogador 1: ");
            Jogador jogador1 = new Jogador(Convert.ToChar(Console.ReadLine()));

            Console.WriteLine("Digite um char identificador para o Jogador 2: ");
            Jogador jogador2 = new Jogador(Convert.ToChar(Console.ReadLine()));

            bool finalizar = false;
            

            do
            {
                Console.WriteLine(jogodavelha.ImprimirTabuleiro());
                Console.WriteLine("Jogador 1: ");
                jogodavelha.Jogar(Convert.ToInt32(Console.ReadLine()), Convert.ToInt32(Console.ReadLine()), jogador1);
               

                Console.WriteLine(jogodavelha.ImprimirTabuleiro());
                finalizar = jogador1.VerificaResultado(tabuleiro);
                if(finalizar){Console.WriteLine("Jogador 1 foi o vencedor!");}
                if(jogodavelha.VerificarSeEmpatou()){finalizar = true; Console.WriteLine("Empate");};

                if(!finalizar)
                {
                    Console.WriteLine("Jogador 2: ");
                    jogodavelha.Jogar(Convert.ToInt32(Console.ReadLine()), Convert.ToInt32(Console.ReadLine()), jogador2);
                    
                    Console.WriteLine(jogodavelha.ImprimirTabuleiro());
                    finalizar = jogador1.VerificaResultado(tabuleiro);
                    if(finalizar){Console.WriteLine("Jogador 2 foi o vencedor!");}
                    if(jogodavelha.VerificarSeEmpatou()){finalizar = true; Console.WriteLine("Empate");};
                }
            }while(!finalizar);
            Console.WriteLine("O jogo finalizou!");

            

          
        }
    }
}
