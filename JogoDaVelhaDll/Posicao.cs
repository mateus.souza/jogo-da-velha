using System;
namespace JogoDaVelhaDll
{
    public class Posicao
    { 
        private int x;
        private int y; 
        private char simbolo;   

        public Posicao(int x, int y, char simbolo)
        {
            this.x = x;
            this.y = y;
            this.simbolo = simbolo;
        }   
        public void MarcarTabuleiro(Tabuleiro tabuleiro)
        {
           tabuleiro.armazenarPosicao(x,y, simbolo);
        }

        

    }
}