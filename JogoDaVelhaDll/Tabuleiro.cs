using System;
namespace JogoDaVelhaDll
{
    public class Tabuleiro
    {
        private char[,] posicoes = new char[3,3];
        private int quantidadeMarcada=0;

        public void armazenarPosicao(int x, int y, char simbolo)
        {
            validarMarcacao(x,y);
            posicoes[x,y] = simbolo;
            quantidadeMarcada = quantidadeMarcada + 1;
        }

        private void validarMarcacao(int x, int y)
        {
       
            if(ValorIvlaidos(x,y))
            {
                throw new IndexOutOfRangeException("Posição não é válida!");
            }
                 
            if(!VerificarPosicaoDisponivel(x,y)){
                throw new ArgumentException("Posição já está marcada!");
            }
            
        }

        private bool ValorIvlaidos(int x, int y)
        {
            
            if(x>2 || x<0 || y>2 || y<0 )
            {
                return true;
            }

            return false;
        }

        private bool VerificarPosicaoDisponivel(int x, int y)
        {
            if(posicoes[x,y] != 0)
            {
                return false;
            }

            return true;
        }

        public bool VerificaSeGanhou()
        {    
            if(VerificarLinhas()){return true;}
            return false;
        }

        public bool VerificarSeEmpatou()
        {
            if(!(quantidadeMarcada is 9))
            {
                return false;
            }
            return true;
        }

        private bool VerificarLinhas()
        {
            for(int posicao=0; posicao<3; posicao++)
            {
                if(posicoes[posicao,0] != 0 && posicoes[posicao,0] == posicoes[posicao,1] && posicoes[posicao,0] == posicoes[posicao,2]){return true;}
                if(posicoes[0,posicao] != 0 && posicoes[0,posicao] == posicoes[1,posicao] && posicoes[0,posicao] == posicoes[2,posicao]){return true;}
                if(posicao<2 && posicoes[0,posicao*2] != 0 && posicoes[0,posicao*2] == posicoes[1,1] && posicoes[0,posicao*2] == posicoes[2,2-(2*posicao)]){return true;}
            }
            return false;
        }


        public string ImprimirTabuleiro()
        {
            return $"__{posicoes[0,0]}__|__{posicoes[0,1]}__|__{posicoes[0,2]}__\n" +
                   $"__{posicoes[1,0]}__|__{posicoes[1,1]}__|__{posicoes[1,2]}__\n" +
                   $"__{posicoes[2,0]}__|__{posicoes[2,1]}__|__{posicoes[2,2]}  \n\n"; 
        }
    
    }
}