
namespace JogoDaVelhaDll
{
    public interface IJogoDaVelha
    {
         public void Jogar(int x, int y, Jogador jogador);      
         public bool VerificarResultado(Jogador jogador);  
         public string ImprimirTabuleiro(); 
         public bool VerificarSeEmpatou();
        
    }
}