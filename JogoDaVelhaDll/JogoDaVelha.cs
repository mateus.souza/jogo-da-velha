
namespace JogoDaVelhaDll
{
    public class JogoDaVelha :IJogoDaVelha
    {
        private Tabuleiro tabuleiro = new Tabuleiro();
        public void Jogar(int x, int y, Jogador jogador)
        {
             jogador.Jogar(x, y, tabuleiro);
        }

        public bool VerificarResultado(Jogador jogador)
        {
            return jogador.VerificaResultado(tabuleiro);
        }

        public string ImprimirTabuleiro()
        {
            return tabuleiro.ImprimirTabuleiro();
        }

        public bool VerificarSeEmpatou()
        {
            return tabuleiro.VerificarSeEmpatou();
        }

        public static IJogoDaVelha Criar()
        {
            return new JogoDaVelha();
        }

    }
}