namespace JogoDaVelhaDll
{
    public class Jogador
    {
       private char simbolo;
       public Jogador(char simbolo)
       {
           this.simbolo = simbolo;
       }
       public void Jogar(int x, int y, Tabuleiro tabuleiro)
       {
           new Posicao(x,y, simbolo).MarcarTabuleiro(tabuleiro);
       }  

       public bool VerificaResultado(Tabuleiro tabuleiro)
       {
           return tabuleiro.VerificaSeGanhou();
       }        
    }
}